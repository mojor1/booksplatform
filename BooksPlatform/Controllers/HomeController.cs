﻿using BooksPlatform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BooksPlatform.Controllers
{
    public class HomeController : Controller
    {
        BooksPlatformContext db = new BooksPlatformContext();
        public ActionResult Index()
        {
            ViewBag.Genres = db.Genres;
            ViewBag.Books = db.Books;
            ViewBag.Authors = db.Users;
            // возвращаем представление
            return View();
        }

        [HttpGet]
        public ActionResult Authorization(string email, string password)
        {
            foreach(var it in db.Users)
            {
                if(it.Email==email && it.Password == password)
                {
                    ViewBag.UserId = it.UserId;
                    return View();
                }
            }
            return ErrorAuthorization(email, password);
        }

        public ActionResult ErrorAuthorization(string name,string password)
        {
            return View();
        }

    }
}