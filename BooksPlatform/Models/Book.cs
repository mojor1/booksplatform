﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BooksPlatform.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; } // Author
        public bool IsPublic { get; set; }
        public bool IsActive { get; set; } // Deleted?
    }
}