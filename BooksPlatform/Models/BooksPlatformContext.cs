﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BooksPlatform.Models
{
    public class BooksPlatformContext:DbContext
    {
        public BooksPlatformContext() : base("BooksPlatformContext") { }
        public DbSet<Book> Books { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<GenersInBook> GetGenersInBooks { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<User> Users { get; set; }

    }

}