﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BooksPlatform.Models
{
    public class Chapter
    {
        public int ChapterId { get; set; }
        public int BookId { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int Number { get; set; }
        public bool IsPublic {get; set;}
        public bool IsActive { get; set; } // Deleted?
    }
}