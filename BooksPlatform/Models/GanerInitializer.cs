﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BooksPlatform.Models
{
    public class GenerInitializer : DropCreateDatabaseAlways<BooksPlatformContext>
    {
        protected override void Seed(BooksPlatformContext db)
        {
            db.Genres.Add(new Genre { Name = "комедия" });
            db.Genres.Add(new Genre { Name = "трагедия" });
            db.Genres.Add(new Genre{ Name = "драмма" });
            db.Genres.Add(new Genre{ Name = "ужасы" });

            base.Seed(db);
        }
    }
}