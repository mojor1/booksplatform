﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BooksPlatform.Models
{
    public class GenersInBook
    {
        public int Id { get; set; }
        public int GenerId { get; set; }
        public int BookId { get; set; }
        public bool IsActive { get; set; } // Deleted?
    }
}